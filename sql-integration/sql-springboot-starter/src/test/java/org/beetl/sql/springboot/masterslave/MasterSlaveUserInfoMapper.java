package org.beetl.sql.springboot.masterslave;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.springboot.UserInfo;

public interface MasterSlaveUserInfoMapper extends BaseMapper<UserInfo> {
}
